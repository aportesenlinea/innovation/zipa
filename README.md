<h1 style="font-size: 3em; line-height: 1.2;" align="center"> Zipa </h1> <br>
<p align="center">
  <a href="http://osaka02s:8088/">
    <img alt="Zipa" title="Zipa" src="https://www.aportesenlinea.com/img/logoAEL70.png" style="width: 12em">
  </a>
</p>

<h4 style="color: #838383" align="center">
  Gestiona facilmente la capacidad de tu equipo.
</h4>

<p>
</p>

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->

# Tabla de Contenidos

- [Introducción](#h1-introducci-n)
- [Pre-requisitos](#h1-pre-requisitos)
- [Primeros pasos](#h1-primeros-pasos)
- [Build & Test](#h1-build-amp-test)
- [Funcionalidades](#h1-funcionalidades)
- [Retroalimentación](#h1-retroalimentaci-n)
- [Colaboradores](#h1-colaboradores)
- [Licencia](#h1-licencia)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Introducción

Zipa es un sistema de gestión de capacidad de equipos de trabajo facilitando la asignación eficiente de tareas. Esto se logra gracias a la recopilación de información de los intereses y la experiencia de los colaboradores y del análisis del historial de tareas finalizadas del equipo.

<p align="center">
  <img src = "http://osaka02s:8088/assets/img/zipa.jpg" style="width: 50vw;border: 1px solid #d4d4d4;">
</p>
<p></p>

Este sistema está conformado por los siguientes módulos:

<p>
</p>

#### - [Zipa-Web](https://dev.azure.com/jjimenezael/Zipa/_git/zipa-web)

[![Build Status](https://img.shields.io/azure-devops/build/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1?logo=Azure%20Pipelines&style=flat-square)](https://dev.azure.com/jjimenezael/Zipa/_build?definitionId=1&_a=summary) ![Azure DevOps releases](https://img.shields.io/azure-devops/release/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1/2?logo=Azure%20Pipelines&style=flat-square) [![Azure DevOps tests](https://img.shields.io/azure-devops/tests/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/4?logo=Azure%20Pipelines&style=flat-square)](https://dev.azure.com/jjimenezael/Zipa/_test/analytics?definitionId=4&contextType=build) [![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?logo=Azure%20Pipelines&style=flat-square)](https://coveralls.io/github/gitpoint/git-point)

#### - [Zipa-Cuestionarios](https://dev.azure.com/jjimenezael/Zipa/_git/zipa-cuestionarios)

[![Build Status](https://img.shields.io/azure-devops/build/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1?logo=Azure%20Pipelines&style=flat-square)](https://dev.azure.com/jjimenezael/Zipa/_build?definitionId=4&_a=summary) ![Azure DevOps releases](https://img.shields.io/azure-devops/release/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1/2?logo=Azure%20Pipelines&style=flat-square) [![Azure DevOps tests](https://img.shields.io/azure-devops/tests/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/4?logo=Azure%20Pipelines&style=flat-square)](https://dev.azure.com/jjimenezael/Zipa/_test/analytics?definitionId=4&contextType=build) [![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?logo=Azure%20Pipelines&style=flat-square)](https://coveralls.io/github/gitpoint/git-point)

#### - [Zipa-Identidades](https://dev.azure.com/jjimenezael/Zipa/_git/zipa-identidades-api)

[![Build Status](https://img.shields.io/azure-devops/build/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1?logo=Azure%20Pipelines&style=flat-square)](https://dev.azure.com/jjimenezael/Zipa/_build?definitionId=4&_a=summary) ![Azure DevOps releases](https://img.shields.io/azure-devops/release/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1/2?logo=Azure%20Pipelines&style=flat-square) [![Azure DevOps tests](https://img.shields.io/azure-devops/tests/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/4?logo=Azure%20Pipelines&style=flat-square)](https://dev.azure.com/jjimenezael/Zipa/_test/analytics?definitionId=4&contextType=build) [![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?logo=Azure%20Pipelines&style=flat-square)](https://coveralls.io/github/gitpoint/git-point)

#### - [Zipa-Directorio-Activo](https://dev.azure.com/jjimenezael/Zipa/_git/zipa-directorio-activo-api)

[![Build Status](https://img.shields.io/azure-devops/build/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1?logo=Azure%20Pipelines&style=flat-square)](https://dev.azure.com/jjimenezael/Zipa/_build?definitionId=4&_a=summary) ![Azure DevOps releases](https://img.shields.io/azure-devops/release/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1/2?logo=Azure%20Pipelines&style=flat-square) [![Azure DevOps tests](https://img.shields.io/azure-devops/tests/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/4?logo=Azure%20Pipelines&style=flat-square)](https://dev.azure.com/jjimenezael/Zipa/_test/analytics?definitionId=4&contextType=build) [![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?logo=Azure%20Pipelines&style=flat-square)](https://coveralls.io/github/gitpoint/git-point)

#### - [Zipa-Indicadores](https://dev.azure.com/jjimenezael/Zipa/_git/zipa-indicadores)

[![Build Status](https://img.shields.io/azure-devops/build/jjimenezael/e06de833-0060-454a-b57a-183c9b1fa250/1?style=flat-square)](https://travis-ci.org/gitpoint/git-point) [![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?style=flat-square)](https://coveralls.io/github/gitpoint/git-point) [![All Contributors](https://img.shields.io/badge/all_contributors-73-orange.svg?style=flat-square)](./CONTRIBUTORS.md) [![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com) [![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](http://commitizen.github.io/cz-cli/) [![Gitter chat](https://img.shields.io/badge/chat-on_gitter-008080.svg?style=flat-square)](https://gitter.im/git-point)

<p>
</p>

# Pre-requisitos

Para poder trabajar en este proyecto es necesario tener las siguientes herramientas:

- Un editor de texto (VS Code, Atom, etc.) o un IDE (Visual Studio, Jetbrains, etc)
- Tener instalado docker
- angular-cli
- Node.js

# Primeros pasos

Para iniciar a trabajar con el proyecto en general clone este repositorio he internamente clone los demas repositorios del proyecto.

```bash
git clone 'https://jjimenezael@dev.azure.com/jjimenezael/Zipa/_git/zipa-cuestionarios'
git clone 'https://jjimenezael@dev.azure.com/jjimenezael/Zipa/_git/zipa-directorio-activo-api'
git clone 'https://jjimenezael@dev.azure.com/jjimenezael/Zipa/_git/zipa-identidades-api'
git clone 'https://jjimenezael@dev.azure.com/jjimenezael/Zipa/_git/zipa-indicadores'
git clone 'https://jjimenezael@dev.azure.com/jjimenezael/Zipa/_git/zipa-web'
```

# Build & Test

Para poner en marcha el proyecto debe en primer lugar compilar el proyecto de zipa-web:

```bash
cd zipa-web
npm install
npm run build:test
```

Luego en el root de este repositorio ejecute el comando `docker-compose up --build`.

```bash
cd ..
docker-compose up --build
```

# Funcionalidades

Algunas de las cosas que puede hacer con Zipa:

- Registrar información de la experiencia de los colaboradores.
- Registrar los conocimientos e intereses de los colaboradores.
- Visualizar tablas con la información ponderada de experiencia, conocimientos e intereses de todo el equipo.
- Cargar un archivo con el historial de tareas ejecutadas por el equipo.
- Calcular indicadores de medición por colaborador de forma automática.
- Registrar indicadores de forma manual.
- Visualizar historial de indicadores de los colaboradores.
- Visualizar los indicadores por fecha del equipo.

# Retroalimentación

No dude en enviarnos sus comentarios o presentar un problema al correo lavila@aportesenlinea.com. Las solicitudes de funciones son siempre bienvenidas. Si desea contribuir, ¡eche un vistazo rápido a las pautas!

Si hay algo sobre lo que le gustaría hablar, ¡no dude en unirse a nuestro chat de [Teams](https://teams.microsoft.com/l/team/19%3ae8f568622be34f37991d973c13943900%40thread.skype/conversations?groupId=0a09b3b6-24e0-45e9-811b-ccad1c3e70b4&tenantId=10a76712-94f6-46a2-9155-31bd8b76f937)!

# Colaboradores

¡Gracias a todos nuestros colaboradores! 🙏 [[Become a contributor](https://opencollective.com/git-point#backer)]

<a href="https://opencollective.com/git-point#backers" target="_blank"><img src="https://opencollective.com/git-point/backers.svg?width=890"></a>

<!--
# Acknowledgments

Thanks for belive in us. -->

# Licencia

Copyright © 2020 Aportes en Línea. Todos los derechos reservados.
